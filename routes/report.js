const express = require('express');
const mysql = require("mysql");
const router = express.Router();

const db = mysql.createConnection({
    host: process.env.DATABASE_HOST,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE
});

router.get('/', (req, res) => {
    let sql = "SELECT transactions.id,books.isbn,books.name AS book_name,"+
                "categories.name AS category,categories.subcategory,members.name AS member_name,"+
                "members.email,transactions.borrow_date,transactions.return_date FROM transactions "+
                "INNER JOIN members ON transactions.member_name = members.name "+
                "INNER JOIN books ON transactions.book_name = books.name "+
                "INNER JOIN categories ON books.category_id = categories.id ORDER BY id ASC";
    let query = db.query(sql, (err, rows) => {
        if(err) throw err;
        res.render('sites/report/index', {
            title: "Laporan Data Perpustakaan",
            reports : rows
        });
    });
});

module.exports = router;