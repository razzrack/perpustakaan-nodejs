const express = require('express');
const mysql = require("mysql");
const router = express.Router();

const db = mysql.createConnection({
    host: process.env.DATABASE_HOST,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE
});

router.get('/', (req, res) => {
    let sql = "SELECT * FROM transactions";
    let query = db.query(sql, (err, rows) => {
        if(err) throw err;
        res.render('sites/transaction/index', {
            title: "Daftar Transaksi Buku",
            transactions : rows
        });
    });
});

router.get('/borrow', (req, res) => {
    res.render('sites/transaction/borrow', {
        title: "Peminjaman Buku"
    });
});

router.post('/borrow', (req, res) => {
    let data = {member_name: req.body.member_name,
                book_name: req.body.book_name,
                borrow_date: req.body.borrow_date};
    let sql = "INSERT INTO transactions SET ?";
    let query = db.query(sql, data, (err, results) => {
        if(err) throw err;
        res.redirect("/transactions/");
    });
});

router.get('/return/:id',(req, res) => {
    const id = req.params.id;
    let sql = `SELECT * FROM transactions WHERE id = ${id}`;
    let query = db.query(sql,(err, result) => {
        if(err) throw err;
        res.render('sites/transaction/return', {
            title : "Pengembalian Buku",
            transaction : result[0]
        });
    });
});

router.post('/return',(req, res) => {
    const id = req.body.id;
    let sql = "UPDATE transactions SET return_date='"+req.body.return_date
                +"' where id ="+id;
    let query = db.query(sql,(err, results) => {
      if(err) throw err;
      res.redirect('/transactions/');
    });
});

router.get('/delete/:id',(req, res) => {
    const id = req.params.id;
    let sql = `DELETE FROM transactions WHERE id = ${id}`;
    let query = db.query(sql,(err, results) => {
        if(err) throw err;
        res.redirect('/transactions/');
    });
});

module.exports = router;