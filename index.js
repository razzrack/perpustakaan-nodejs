// import module
const express = require("express");
const path = require("path")
const mysql = require("mysql");
const dotenv = require("dotenv");
const cookieParser = require("cookie-parser");
// konfigurasi dotenv untuk database dan jwt
dotenv.config({ path: './.env'});
// inisialisasi express dengan nama app
const app = express();
// inisialisasi db mysql dengan dotenv
const db = mysql.createConnection({
    host: process.env.DATABASE_HOST,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE
});
// import bootstrap pada folder public
const publicDirectory = path.join(__dirname, './public');
app.use(express.static(publicDirectory));

// parsing URLencoded
app.use(express.urlencoded({ extended: false }));
// parsing JSON
app.use(express.json());
// parsing cookieparser
app.use(cookieParser());

// konfigurasi template engine ejs
app.set('view engine', 'ejs');

// menampilkan console db terhubung atau tidak
db.connect( (error) => {
    if (error) {
        console.log(error);
    } else {
        console.log("MySQL Connected..")
    }
})

// inisialisasi route
app.use('/', require('./routes/pages'));
app.use('/auth', require('./routes/auth'));
app.use('/home', require('./routes/home'));
app.use('/books', require('./routes/book'));
app.use('/categories', require('./routes/category'));
app.use('/members', require('./routes/member'));
app.use('/transactions', require('./routes/transaction'));
app.use('/reports', require('./routes/report'));

// menampilkan port 3001
app.listen(3001, () => {
    console.log("Server start on port 3001");
});