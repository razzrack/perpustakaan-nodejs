const express = require('express');
const homeController = require('../controllers/home');

const router = express.Router();

// route dashboard
router.get('/dashboard', (req, res) => {
    res.render('sites/dashboard');
});
// route logout
router.get('/logout', (req, res) => {
    res.clearCookie('jwt');
    res.redirect('/');
});

module.exports = router;